package com.smartdealsng.business.authentication.boundary;

import com.smartdealsng.business.authentication.control.PasswordEncoder;
import com.smartdealsng.business.partners.entity.LiveBusiness;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import com.smartdealsng.business.authentication.control.AuthenticationStore;
import com.smartdealsng.business.authentication.model.PasswordInfo;
import com.smartdealsng.business.authentication.model.TempBizVerificationToken;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.WebApplicationException;

/**
 * This class serves as a boundary for Registering new Business, which is 
 * Temporary until they are confirmed and upgraded to a live Business
 * 
 * @author tunji@smartdeals.com.ng
 */
@Stateless
public class Authenticator {
    
    @PersistenceContext
    EntityManager manager;
    
    @Inject
    Logger logger;
    
    @Inject
    AuthenticationStore authStore;
    
    @Inject
    PasswordEncoder encoder;
    
    public TemporaryBusiness register(TemporaryBusiness business) {
        try {
            logger.info("---received temporary business for registration---");
            final String email = business.getEmail();
            final String businessName = business.getBusinessName();
            if (businessNameExists(businessName, TemporaryBusiness.class)) {
                logger.warning("---business name has been taken---");
                throw new InvalidDetailsException("business name already exists");
            }
            if (emailExists(email, TemporaryBusiness.class)) {
                logger.warning("---email already exists---");
                throw new InvalidDetailsException("email already exists");
            }
            this.manager.persist(business);
        } catch (ConstraintViolationException ce) {
            logger.info("invalid registration details");
            throw new InvalidDetailsException(ce.getLocalizedMessage());
        }
        return business;
    }
    
    private <T> boolean emailExists(String email, java.lang.Class<T> table) {
        return this.manager.createQuery("SELECT COUNT(t) FROM " + table.getSimpleName() + " t "
                + " WHERE t.email = :email ", Long.class)
                .setParameter("email", email)
                .getSingleResult() != 0;
    }
    
    private <T> boolean businessNameExists(String businessName, java.lang.Class<T> table) {
        return this.manager.createQuery("SELECT COUNT(t) FROM " + table.getSimpleName() + " t "
                + " WHERE t.businessName = :businessName ", Long.class)
                .setParameter("businessName", businessName)
                .getSingleResult() != 0;
    }
    
    private String generateLiveBusinessID(String businessName) {
        final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final Random r = new Random();
        final Calendar date = Calendar.getInstance();
        final String format = businessName + "" + date.getWeekYear() + "";
        final Long totalStaff = this.manager.createQuery("SELECT COUNT(l) FROM LiveBusiness l ", Long.class)
                .getSingleResult();
        final StringBuilder result = new StringBuilder(Long.toString(totalStaff + 1));
        for (int i = 0; i < 2; i++) {
            result.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        return format + result;
    }
    
    /**
     * This method verifies that a token indeed exists in the database or is not expired
     * on verification of a token associated with {@link TemporaryBusiness} it is 
     * upgraded to a {@link LiveBusiness}. per default, the username and password written
     * to the database are the Business email and password.
     * 
     * @param token the token to check for verification
     * @return the generated business ID of a business that has gone Live.
     */
    public String verify(String token) {
        logger.info("---received token for verification---");
        final TempBizVerificationToken verificationToken = this.authStore.findTempBizByToken(token);
        final TemporaryBusiness tempBiz = verificationToken.getTempBiz();
        if (LocalDateTime.now().isAfter(verificationToken.getExpirationTime())) {
            this.authStore.deleteExpiredToken(token);
            this.manager.remove(tempBiz);
            logger.severe("---token expired---");
            throw new WebApplicationException("Token expired");
        }
        final LiveBusiness liveBusiness = new LiveBusiness(tempBiz);
        try {
            liveBusiness.setVerificationCode(verificationToken.getVerificationToken());
            liveBusiness.setBusinessID(generateLiveBusinessID(liveBusiness.getBusinessName()));
            this.manager.persist(liveBusiness);
            final SecurityInfo securityInfo = new SecurityInfo(liveBusiness.getEmail(), encoder.digestPassword(liveBusiness.getEmail()), liveBusiness.getVerificationCode());
            this.manager.persist(securityInfo);
            logger.info("---token verified---");
        } catch (ConstraintViolationException cve) {
            logger.severe("invalid details");
            throw new InvalidDetailsException(cve.getLocalizedMessage());
        }
        return liveBusiness.getBusinessID();
    }
}

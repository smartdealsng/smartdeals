package com.smartdealsng.business.authentication.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *This class acts as a DTO for transporting credentials involved when making
 * a request for a password change.
 * 
 * @author tunji@smartdeals.com.ng
 */
public class PasswordInfo {
    
    public String oldPassword;
    public String newPassword;
    public String confirmPassword;
    
    public PasswordInfo() {
        
    }
    
    public PasswordInfo(String oldPassword, String newPassword, String confirmPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }
    
}

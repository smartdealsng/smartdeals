package com.smartdealsng.business.authentication.model;

import static com.smartdealsng.business.authentication.model.SecurityInfo.FIND_BY_TOKEN;
import static com.smartdealsng.business.authentication.model.SecurityInfo.FIND_BY_USERNAME_PASSWORD;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * Entity class representing all security info.
 *
 * @author tunji@smartdeals.com.ng
 */
@Entity
@Table(name = "security_info", uniqueConstraints = {@UniqueConstraint(name = "UK_security_info_username_password_token",
        columnNames = {"username", "password", "token"})})
@NamedQueries({
    @NamedQuery(name = FIND_BY_USERNAME_PASSWORD, query = "SELECT s FROM SecurityInfo s WHERE s.username =:username AND s.password =:password"),
    @NamedQuery(name = FIND_BY_TOKEN, query = "SELECT s FROM SecurityInfo s WHERE s.verificationToken =:token ")
})
public class SecurityInfo {
    
    public static final String PREFIX = "com.smartdealsng.security.model.UserSecurityInfo";
    public static final String FIND_BY_USERNAME_PASSWORD = PREFIX + ".findByUsernamePassword";
    public static final String FIND_BY_TOKEN = PREFIX + ".findByToken";
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    
    @Column(name = "username", nullable = false)
    @NotNull
    private String username;
    
    @Column(name = "password", nullable = false)
    @NotNull
    private String password;
    
    @Column(name = "token", nullable = false)
    @NotNull
    private String verificationToken;
    
    public SecurityInfo() {
        
    }
    
    public SecurityInfo(String username, String password, String verificationToken) {
        this.username = username;
        this.password = password;
        this.verificationToken = verificationToken;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public String getUserName() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String getVerificationToken() {
        return this.verificationToken;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
}

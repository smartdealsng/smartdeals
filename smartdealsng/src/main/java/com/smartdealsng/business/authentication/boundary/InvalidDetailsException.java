
package com.smartdealsng.business.authentication.boundary;

import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@ApplicationException(rollback = true)
public class InvalidDetailsException extends WebApplicationException {

    public InvalidDetailsException(String message) {
        super(Response.status(Status.BAD_REQUEST)
                .header("x-cause", message)
                .build());
    }
    
}

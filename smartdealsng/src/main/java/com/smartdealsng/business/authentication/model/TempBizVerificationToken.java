package com.smartdealsng.business.authentication.model;

import static com.smartdealsng.business.authentication.model.TempBizVerificationToken.FIND_TEMP_BIZ_BY_TOKEN;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Entity class representing verification token generated for when a {@link TemporaryBusiness)
 * wants to confirm their signup
 *
 * @author tunji@smartdeals.com.ng
 */
@Entity
@Table(name = "temp_biz_verification_token")
@NamedQuery(name = FIND_TEMP_BIZ_BY_TOKEN, query = "SELECT t FROM TempBizVerificationToken t WHERE t.verificationToken = :token" )
public class TempBizVerificationToken {
    
    private static final String PREFIX = "com.smartdealsng.security.model.TempBizVerificationToken";
    public static final String FIND_TEMP_BIZ_BY_TOKEN = PREFIX + ".findByToken";
    
    @Id
    private Long id;
    
    @NotNull
    @OneToOne(targetEntity = TemporaryBusiness.class, fetch = FetchType.EAGER)
    @MapsId
    @JoinColumn(nullable = false, name = "temp_biz_id")
    private TemporaryBusiness tempBiz;
    
    @Column(name = "token", nullable = false, updatable = false, unique = true)
    @NotBlank
    private String verificationToken;
    
    @NotNull
    @Column(name = "expiration_date", nullable = false, updatable = false)
    private LocalDateTime expirationTime;
    
    public TempBizVerificationToken() {
        
    }
    
    public TempBizVerificationToken(@NotBlank String verificationToken, @NotNull LocalDateTime expirationTime) {
        this.verificationToken = verificationToken;
        this.expirationTime = expirationTime;
    }

    public Long getId() {
        return id;
    }

    public TemporaryBusiness getTempBiz() {
        return tempBiz;
    }

    public String getVerificationToken() {
        return verificationToken;
    }

    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }
    
    public void setTempBiz(TemporaryBusiness tempBiz) {
        this.tempBiz = tempBiz;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.tempBiz);
        hash = 73 * hash + Objects.hashCode(this.verificationToken);
        hash = 73 * hash + Objects.hashCode(this.expirationTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TempBizVerificationToken other = (TempBizVerificationToken) obj;
        if (!Objects.equals(this.verificationToken, other.verificationToken)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.tempBiz, other.tempBiz)) {
            return false;
        }
        if (!Objects.equals(this.expirationTime, other.expirationTime)) {
            return false;
        }
        return true;
    }
}

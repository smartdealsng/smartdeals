package com.smartdealsng.business.authentication.control;

import com.smartdealsng.business.authentication.model.SecurityInfo;
import static com.smartdealsng.business.authentication.model.SecurityInfo.FIND_BY_USERNAME_PASSWORD;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import com.smartdealsng.business.authentication.model.TempBizVerificationToken;
import static com.smartdealsng.business.authentication.model.SecurityInfo.FIND_BY_TOKEN;
import static com.smartdealsng.business.authentication.model.TempBizVerificationToken.FIND_TEMP_BIZ_BY_TOKEN;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@RequestScoped
public class AuthenticationStore {
    
    @PersistenceContext
    EntityManager manager;
    
    @Inject
    PasswordEncoder encoder;
    
    @Inject
    Logger logger;
    
    /**
     * this method saves a verification token for a temporary business to the database
     * the token is set for expiration after a duration of one hour.
     * 
     * @param business the (@link TemporaryBusiness} associated with the token
     * @param token the verification token
     */
    @Transactional
    public void saveTemporaryBusinessToken(TemporaryBusiness business, String token) {
        final LocalDateTime expirationDate = LocalDateTime.now().plusHours(1L);
        final TempBizVerificationToken verificationToken = new TempBizVerificationToken(token, expirationDate);
        verificationToken.setTempBiz(business);
        this.manager.persist(verificationToken);
    }
    
    public TempBizVerificationToken findTempBizByToken(String token) {
        logger.info("retrieving Temporary Business Verification Token");
        final List<TempBizVerificationToken> verificationToken = this.manager.
                createNamedQuery(FIND_TEMP_BIZ_BY_TOKEN, TempBizVerificationToken.class).
                setParameter("token", token).
                setMaxResults(1).
                getResultList();
        if (verificationToken == null || verificationToken.isEmpty()) {
            logger.severe("Temorary Business Verification token not found");
            throw new WebApplicationException(Response.status(Status.NO_CONTENT).build());
        }
        return verificationToken.get(0);
    }
    
    public void deleteExpiredToken(String token) {
        final TempBizVerificationToken verificationToken = findTempBizByToken(token);
        this.manager.remove(verificationToken);
    }
    
    public SecurityInfo getSecurityInfo(String username, String password) {
        logger.info("verifying security information for partner: " + username);
        final List<SecurityInfo> securityInfo = this.manager
                .createNamedQuery(FIND_BY_USERNAME_PASSWORD, SecurityInfo.class)
                .setParameter("username", username)
                .setParameter("password", encoder.digestPassword(password))
                .setMaxResults(1)
                .getResultList();
        if (securityInfo == null || securityInfo.isEmpty()) {
            logger.info("no security information associated with username: " + username);
            throw new AuthenticationException("Incorrect username or password");
        }
        return securityInfo.get(0);
    }
    
    public SecurityInfo findByUsername(String username) {
        final List<SecurityInfo> securityInfo = this.manager
                .createQuery("SELECT s FROM SecurityInfo s WHERE s.username =:username", SecurityInfo.class)
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList();
        if(securityInfo == null || securityInfo.isEmpty()) {
            throw new WebApplicationException(Response.status(Status.NO_CONTENT).build());
        }
        return securityInfo.get(0);
    }
    
    public SecurityInfo findByToken(String token) {
        final List<SecurityInfo> securityInfo = this.manager.createNamedQuery(FIND_BY_TOKEN, SecurityInfo.class)
                .setParameter("token", token)
                .setMaxResults(1)
                .getResultList();
        if (securityInfo == null || securityInfo.isEmpty()) {
            throw new NotAuthorizedException("Not Authorized");
        }
        return securityInfo.get(0);
    }
}

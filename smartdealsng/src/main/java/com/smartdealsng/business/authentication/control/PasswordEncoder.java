package com.smartdealsng.business.authentication.control;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ws.rs.WebApplicationException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class PasswordEncoder {
    
    private static final String SALT = "SLCR";

    public String digestPassword(String input) {
        final String saltedInput = SALT + input;
        byte[] bytes = saltedInput.getBytes(Charset.forName("UTF-8"));
        for (int i = 1; i <= 100; ++i) {
            bytes = computeSHA256Hash(bytes);
        }
        return DatatypeConverter.printHexBinary(computeSHA256Hash(bytes));
    }

    private static byte[] computeSHA256Hash(byte[] input) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ne) {
            throw new WebApplicationException(ne);
        }
        digest.update(input);
        return digest.digest();
    }
}

package com.smartdealsng.business.authentication.model;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.ws.rs.NameBinding;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@NameBinding
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface VerificationTokenNeeded {
    
}

package com.smartdealsng.business.authentication.control;

import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@ApplicationException(rollback = true)
public class AuthenticationException extends WebApplicationException {

    public AuthenticationException(String message) {
        super(Response.status(Status.UNAUTHORIZED)
                .header("reason: ", message)
                .build());
    }
    
}

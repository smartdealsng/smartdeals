package com.smartdealsng.business.authentication.control;

import com.smartdealsng.business.partners.entity.TempBizRegCompletedEvent;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import java.util.UUID;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

/**
 *
 * @author tuni@smartdeals.com.ng
 */
public class EventObserver {
    
    @Inject
    AuthenticationStore authStore;
    
    @Inject
    MailSender mailSender;
    
    public void onTempBizRegistration(@Observes TempBizRegCompletedEvent event) {
        confirmTempBizSignup(event.getTemporaryBusiness(), event.getBaseUrl());
    }
    
    private void confirmTempBizSignup(TemporaryBusiness tempBiz, String baseurl) {
        final String token = UUID.randomUUID().toString();
        this.authStore.saveTemporaryBusinessToken(tempBiz, token);
        this.mailSender.sendConfirmationEmailToTempBiz(tempBiz, token, baseurl);
    }
}
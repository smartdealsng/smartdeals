package com.smartdealsng.business.authentication.boundary;

import com.smartdealsng.business.authentication.control.AuthenticationStore;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import com.smartdealsng.business.authentication.model.VerificationTokenNeeded;
import java.io.IOException;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * This class intercepts every request made to any resource bounded by {@link VerificationTokenNeeded}
 * and verifies if the token passed alongside the Authorization header is valid. Value of the authorization
 * header starts with Basic
 *
 * @author tunji@smartdeals.com.ng
 */
@Provider
@VerificationTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class TokenFilter implements ContainerRequestFilter {
    
    @Inject
    AuthenticationStore authStore;
    
    @Inject
    Logger logger;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        logger.info("authorizationHeader: " + authorizationHeader);
        if (authorizationHeader == null || !authorizationHeader.startsWith("Basic ")) {
            logger.severe("invalid authorizationHeader: " + authorizationHeader);
            throw new NotAuthorizedException("Authorization header must be provided");
        }
        String token = authorizationHeader.substring("Basic".length()).trim();
        try {
            final SecurityInfo info = this.authStore.findByToken(token);
            logger.info("#### valid token : " + token + " " + info.getUserName());
        } catch (Exception e) {
            logger.severe("#### invalid token : " + token + e.getMessage());
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        } 
    }
    
}

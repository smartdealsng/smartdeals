package com.smartdealsng.business.authentication.control;

import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.metrics.MetricRegistry;
import org.eclipse.microprofile.metrics.annotation.RegistryType;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@Stateless
public class MailSender {

    @Inject
    Logger logger;

    @Inject
    @ConfigProperty(name = "mail.smtp.user")
    private String username;

    @Inject
    @ConfigProperty(name = "mail.smtp.password")
    private String password;

    @Inject
    @ConfigProperty(name = "mail.smtps.auth", defaultValue = "true")
    private String smtpAuth;

    @Inject
    @ConfigProperty(name = "mail.smtp.starttls.enable", defaultValue = "true")
    private String tlsEnable;

    @Inject
    @ConfigProperty(name = "mail.smtp.host")
    private String host;

    @Inject
    @ConfigProperty(name = "mail.smtp.port")
    private String port;

    @Inject
    @ConfigProperty(name = "mail.transport.protocol", defaultValue = "smtps")
    private String protocol;

    @Inject
    @RegistryType(type = MetricRegistry.Type.APPLICATION)
    MetricRegistry registry;

    @Asynchronous
    public void sendConfirmationEmailToTempBiz(TemporaryBusiness tempBiz, String token, String baseurl) {
        final Session session = createSession();
        final Message message = createMessage(session, tempBiz, token, baseurl);
        try {
            final Transport transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            this.registry.counter("emails_delivered_").inc();
        } catch (NoSuchProviderException ex) {
            this.registry.counter("emails_undelivered_").inc();
           logger.severe("invalid email provider");
        } catch (MessagingException ex) {
            this.registry.counter("email_undelivered_").inc();
            logger.severe("error sending message: " + ex.getMessage());
            throw new WebApplicationException(Response.status(Status.SERVICE_UNAVAILABLE)
                    .header("x-cause", ex.getMessage())
                    .build());
        }
    }

    Message createMessage(Session session, TemporaryBusiness tempBiz, String token, String baseurl) {
        final Message message = new MimeMessage(session);
        final String confirmationUrl = baseurl + "/" + token;
        final String content = String.format("Dear %s, %n%nThanks for signing up, kindly verify your email by "
                + "clicking this link %s", tempBiz.getBusinessName(), confirmationUrl);
        try {
            message.setFrom(new InternetAddress("no-reply@smartdeals.com.ng", "SMARTDEALS DISCOUNT CLUB"));
            message.setSubject("New Registration");
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(tempBiz.getEmail()));
            message.setText(content);
            return message;
        } catch (UnsupportedEncodingException | MessagingException ex) {
            Logger.getLogger(MailSender.class.getName()).log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.status(Status.SERVICE_UNAVAILABLE)
                    .header("x-cause", ex.getMessage())
                    .build());
        }
    }

    Session createSession() {
        final Properties props = System.getProperties();
        props.put("mail.transport.protocol", protocol);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.fallback", "false");
        final Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        session.setDebug(true);
        return session;
    }

    public void sendConfirmationEmailToTempUser() {
        //TODO:
    }
}

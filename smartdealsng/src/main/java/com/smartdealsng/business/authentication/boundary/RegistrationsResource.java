package com.smartdealsng.business.authentication.boundary;

import com.smartdealsng.business.authentication.control.AuthenticationException;
import com.smartdealsng.business.authentication.control.AuthenticationStore;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import com.smartdealsng.business.partners.entity.TempBizRegCompletedEvent;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import java.net.URI;
import java.util.logging.Logger;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * 
 * This class server a JAX-RS Resource exposing it's endpoint via REST
 * and JSON.
 *
 * @author tunji@smartdeals.com.ng
 */
@Path("/auth")
@Produces({APPLICATION_JSON, APPLICATION_XML})
@Consumes({APPLICATION_JSON, APPLICATION_XML})
public class RegistrationsResource {
    
    @Inject
    Authenticator authenticator;
    
    @Inject
    AuthenticationStore authStore;
    
    @Inject
    Event<TempBizRegCompletedEvent> rce;
    
    @Context 
    UriInfo info;
    
    @Inject
    Logger logger;
    
    @POST
    @Path("/register")
    public Response registerTemporaryBusiness(JsonObject request) {
        logger.info("---registering new temporary business---");
        final TemporaryBusiness tempBiz = this.authenticator.register(new TemporaryBusiness(request));
        final URI baseuri = info.getBaseUriBuilder().path("/auth/verify").build();
        this.rce.fire(new TempBizRegCompletedEvent(tempBiz, baseuri.toString()));
        logger.info("confirmation email sent");
        logger.info("---registeration completed---");
        return Response.ok().build();
    }
    
    @GET
    @Path("/verify/{token}")
    public Response verifyTemporaryBusiness(@PathParam("token") String token) {
        final String businessID = this.authenticator.verify(token);
        return Response.ok().entity(businessID).build();
    }
    
    @POST
    @Path("/login")
    public Response login(JsonObject request) {
        logger.info("new login request");
        final String username = request.getString("username");
        final String password = request.getString("password");
        if (!(isValid(username, password))) {
            throw new AuthenticationException("invalid username and password");
        }
        final SecurityInfo securityInfo = authStore.getSecurityInfo(username, password);
        return Response.ok().entity("Login successful")
                .header("Authorization", "Basic " + securityInfo.getVerificationToken())
                .build();
    }
    
    boolean isValid(String username, String password) {
        return (username != null && password != null);
    }
    
}

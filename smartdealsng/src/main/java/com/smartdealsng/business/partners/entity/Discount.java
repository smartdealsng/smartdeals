package com.smartdealsng.business.partners.entity;

import java.math.BigDecimal;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity class that represents a smartdeals discount associated with a Live Business
 *
 * @author tunji@smartdeals.com.ng
 */
@Entity(name = "Discount")
@Table(name = "live_business_discount_table")
public class Discount {
    
    @Id
    private Long id;
    
    @Column(name = "active", nullable = false)
    @Enumerated(STRING)
    private DiscountStatus status;
    
    @Column(name = "discount_offered", nullable = false)
    private int discountOffered;
    
    @Column(name = "minimum_spend", nullable = false)
    private String minimumSpend;
    
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id")
    private LiveBusiness liveBusiness;
    
    
    public Discount() {
        
    }
    
    public Discount(JsonObject object) {
        this.status = DiscountStatus.valueOf(object.getString("status"));
        this.discountOffered = object.getInt("discountOffered");
        this.minimumSpend = object.getString("minimum_spend");
    }
    
    public Discount(DiscountStatus status, int discountOffered, String minimumSpend) {
        this.status = status;
        this.discountOffered = discountOffered;
        this.minimumSpend = minimumSpend;
    }
    
    public JsonObject toJson() {
        return Json.createObjectBuilder()
                .add("status", this.status.toString())
                .add("discountOffered", this.discountOffered)
                .add("minimum_spend", this.minimumSpend)
                .build();
    }
    
    public Long getId() {
        return id;
    }

    public DiscountStatus getStatus() {
        return status;
    }

    public void setActive(DiscountStatus status) {
        this.status = status;
    }

    public int getDiscountOffered() {
        return discountOffered;
    }

    public void setDiscountOffered(int disountOffered) {
        this.discountOffered = disountOffered;
    }

    public String getMinimumSpend() {
        return minimumSpend;
    }

    public void setMinimumSpend(String minimumSpend) {
        this.minimumSpend = minimumSpend;
    }
    
    public LiveBusiness getAssociatedLiveBusiness() {
        return liveBusiness;
    }
    
    public void setAssociatedLiveBusiness(LiveBusiness liveBusiness) {
        this.liveBusiness = liveBusiness;
    } 
    
}

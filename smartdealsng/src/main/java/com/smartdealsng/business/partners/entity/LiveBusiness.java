package com.smartdealsng.business.partners.entity;

import static com.smartdealsng.business.partners.entity.DiscountStatus.ACTIVE;
import static com.smartdealsng.business.partners.entity.LiveBusiness.FIND_BY_ID;
import java.time.LocalDateTime;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This Entity class represents a Business that has gone from being temporary 
 * {@link TemporaryBusiness} and is now live.
 *
 * @author tunji@smartdeals.com.ng
 */
@Entity
@Table(name = "live_business")
@NamedQueries({@NamedQuery(name = FIND_BY_ID, query = "SELECT l FROM LiveBusiness l where l.businessID =:businessID")})
public class LiveBusiness {

    private static final String PREFIX = "com.smartdealsng.business.entity.LiveBusiness";
    public static final String FIND_BY_ID = PREFIX + ".findById";

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    
    @Column(name = "business_name", nullable = false)
    private String businessName;
    
    @Column(name = "email", nullable = false)
    @Email
    private String email;
    
    @Column(name = "phoneNumber", nullable = false)
    private String phoneNumber;
    
    @Column(name = "verificationCode", nullable = false)
    private String verificationCode;
    
    @Column(name = "registeredAt", nullable = false)
    private LocalDateTime registeredAt;
    
    @Column(name = "crm_code")
    private String crmCode;
    
    @Column(name = "rc_numberr")
    private String rcNumber;
    
    @Column(name = "business_id", nullable = false, unique = true, updatable = false)
    private String businessID;
    
    @Column(name = "business_address")
    private String businessAddress;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "state")
    private String state;
    
    @Column(name = "country")
    private String country;

    @Column(name = "image_url")
    private String imageUrl;
    
    @Column(name = "instagram_handle")
    private String instagramHandle;
    
    @Column(name = "business_category")
    private String businessCategory;
    
    @Column(name = "business_type")
    private String businessType;
    
    @Column(name = "business_desc")
    private String businessDescription;
    
    @Column(name = "account_enabled")
    private boolean accountEnabled;
    
    @Column(name = "account_active")
    private boolean accountActive;
    
    @Column(name = "partner_status")
    private boolean partnerStatus;
    
    public LiveBusiness() {
        
    }
    
    public LiveBusiness(JsonObject object) {
        this.rcNumber = object.getString("rc_number");
        this.businessAddress = object.getString("address");
        this.city = object.getString("city");
        this.state = object.getString("state");
        this.country = object.getString("country");
        this.instagramHandle = object.getString("instagram_handle");
        this.businessCategory = object.getString("business_category");
        this.businessType = object.getString("business_type");
        this.businessDescription = object.getString("businessDescription");
        this.registeredAt = LocalDateTime.now();
    }
    
    public LiveBusiness(TemporaryBusiness tempBiz) {
        this.businessName = tempBiz.getBusinessName();
        this.email = tempBiz.getEmail();
        this.phoneNumber = tempBiz.getPhone();
        this.registeredAt = LocalDateTime.now();
    }
    
    public JsonObject toJson() {
        return Json.createObjectBuilder().
                add("business_name", this.businessName).
                add("email", this.email).
                add("phone_number", this.phoneNumber).
                add("verification_code", this.verificationCode).
                add("registeredAt", this.registeredAt.toString()).
                add("crm_code", this.crmCode).
                add("businessID", this.businessID).
                add("businessAddress", this.businessAddress).
                add("city", this.city).
                add("state", this.state).
                add("country", this.country).
                add("instagram_handle", this.instagramHandle).
                add("business_category", this.businessCategory).
                add("business_type", this.businessType).
                add("business_description", this.businessDescription).
                add("account_active", this.accountActive).
                add("partner_status", this.partnerStatus).
                build();
    }
    
    public Long getId() {
        return id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(@Email String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public LocalDateTime getRegistratedAt() {
        return registeredAt;
    }

    public void setRegisteredAt(LocalDateTime registeredAt) {
        this.registeredAt = registeredAt;
    }

    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }
    
    public String getBusinessID() {
        return businessID;
    }
    
    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public String getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }
    
    public String getImageURL() {
        return this.imageUrl;
    }
    
    public void setImageURL(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInstagramHandle() {
        return instagramHandle;
    }

    public void setInstagramHandle(String instagramHandle) {
        this.instagramHandle = instagramHandle;
    }

    public String getBusinessCategory() {
        return businessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory = businessCategory;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

   public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
   }
    
    public boolean getAccountEnabled() {
        return this.accountEnabled;
    }
    
    public void setAccountEnabled(DiscountStatus status) {
        switch(status) {
            case ACTIVE:
                this.accountEnabled = true;
                break;
            case INACTIVE:
                this.accountEnabled = false;
                break;
            default:
                this.accountEnabled = false;
        }
    }
    
    public void setAccountActive() {
        if (!this.crmCode.trim().isEmpty()) {
            this.accountActive = true;
        }
        this.accountActive = false;
    }
    
    public boolean getAccountActive() {
        return this.accountActive;
    }
    
    public void setPartnerStatus() {
        if (!(this.accountActive && this.accountEnabled)) {
            this.partnerStatus = false;
        }
        this.partnerStatus = true;
    }
    
    public boolean getPartnerStatus() {
        return this.partnerStatus;
    }
}

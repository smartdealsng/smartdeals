package com.smartdealsng.business.partners.entity;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class TempBizRegCompletedEvent {
    
    private final TemporaryBusiness tempBiz;
    private final String baseUrl;

    public TempBizRegCompletedEvent(TemporaryBusiness tempBiz, String baseUrl) {
        this.tempBiz = tempBiz;
        this.baseUrl = baseUrl;
    }

    public TemporaryBusiness getTemporaryBusiness() {
        return tempBiz;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}

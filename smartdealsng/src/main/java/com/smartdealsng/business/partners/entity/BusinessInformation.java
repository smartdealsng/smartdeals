package com.smartdealsng.business.partners.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class acts as a DTO to update values in the {@link LiveBusiness} entity.
 * all fields are public per default to enable automatic serialization with JSON-B 
 * and is annotated with {@link @XmlRootElement} for when data would be transported
 * via XML
 *
 * @author tunji@smartdeals.com.ng
 */
public class BusinessInformation {
    
    public String businessName;
    public String rcNumber;
    public String email;
    public String phoneNumber;
    public String businessAddress;
    public String instagramHandle;
    public String city;
    public String state;
    public String country;
    public String businessCategory;
    public String businessType;
    public String description;
    
    public BusinessInformation() {
        
    }
    
    public BusinessInformation(String businessName, String rcNumber, String email, String phoneNumber, String businessAddress, 
            String instagramHandle, String city, String state, String country, String businessCategory, String businessType, 
            String description) {
        this.businessName = businessName;
        this.rcNumber = rcNumber;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.businessAddress = businessAddress;
        this.instagramHandle = instagramHandle;
        this.city = city;
        this.state = state;
        this.country = country;
        this.businessCategory = businessCategory;
        this.businessType = businessType;
        this.description = description;
    }
}

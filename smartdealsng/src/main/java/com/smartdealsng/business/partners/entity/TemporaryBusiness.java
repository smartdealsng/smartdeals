package com.smartdealsng.business.partners.entity;

import static com.smartdealsng.business.partners.entity.TemporaryBusiness.FIND_BY_NAME;
import java.time.LocalDateTime;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Email;

/**
 * This class represents a Temporary Business which hasn't gone live on the
 * platform
 *
 * @author tunji@smartdeals.com.ng
 */
@Entity
@Table(name = "temp_business")
@NamedQueries({@NamedQuery(name = FIND_BY_NAME, query = "SELECT t FROM TemporaryBusiness t WHERE t.businessName =:businessName")})
public class TemporaryBusiness {
    
    private static final String PREFIX = "com.smartdealsng.business.entity.TemporaryBusiness";
    public static final String FIND_BY_NAME = PREFIX + ".findByName";
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    
    @Column(name = "business_name", nullable = false)
    private String businessName;
    
    @Column(name = "email", nullable = false)
    @Email
    private String email;
    
    @Column(name = "phoneNumber", nullable = false)
    private String phone;
   
    @Column(name = "registeredAt", nullable = false)
    private LocalDateTime registrationDate;
    
    @Column(name = "CRM_Code")
    private String crmCode;
    
    public TemporaryBusiness() {
        
    }
    
    public TemporaryBusiness(JsonObject object) {
        this.businessName = object.getString("businessName");
        this.email = object.getString("email");
        this.phone = object.getString("phone");
        this.registrationDate = LocalDateTime.now();
        this.crmCode = object.getString("crm_code");
    }
    
    public TemporaryBusiness(String businessName, @Email String email, String phone,
            LocalDateTime registrationDate, String crmCode) {
        this.businessName = businessName;
        this.email = email;
        this.phone = phone;
        this.registrationDate = registrationDate;
        this.crmCode = crmCode;
    }
    
    public JsonObject toJson() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("businessName", this.businessName)
                .add("email", this.email)
                .add("phone", this.phone)
                .add("registeredAt", this.registrationDate.toString())
                .add("crm_code", this.crmCode);
        return builder.build();
    }

    public Long getId() {
        return id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(@Email String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }
    
    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }
    
    public String getCrmCode() {
        return crmCode;
    }
    
}

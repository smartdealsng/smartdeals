package com.smartdealsng.business.partners.boundary;

import com.smartdealsng.business.authentication.model.PasswordInfo;
import com.smartdealsng.business.authentication.model.VerificationTokenNeeded;
import com.smartdealsng.business.partners.entity.BusinessInformation;
import com.smartdealsng.business.partners.entity.Discount;
import java.io.InputStream;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import javax.ws.rs.core.Response;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@Path("/business")
@VerificationTokenNeeded
@Produces({APPLICATION_JSON, APPLICATION_XML})
@Consumes({APPLICATION_JSON, APPLICATION_XML})
public class LiveBusinesResource {

    @Inject
    BusinessAdmin admin;

    @PUT
    @Path("/{businessID}")
    public Response updateBusinessInformation(@PathParam("businessID") String businessID, BusinessInformation document) {
        this.admin.updateLiveBusinessDocument(businessID, document);
        return Response.ok("Document updated").build();
    }

    @POST
    @Path("/{businessID}/discount")
    public Response addDiscountToBusiness(@PathParam("businessID") String businessName, JsonObject request) {
        final String status = this.admin.addDiscount(businessName, new Discount(request));
        return Response.ok(status).build();

    }

    @POST
    @Consumes("*/*")
    @Path("/{businessID}/upload")
    public Response uploadBusinessPicture(@PathParam("businessID") String businessID, InputStream stream) {
        this.admin.uploadImageOrDefault(businessID, stream);
        return Response.ok("Upload Successful").build();
    }
    
    @PUT
    @Path("/{businessID}/changepassword")
    public Response changePassword(@PathParam("businessID") String businessID, PasswordInfo info) {
        this.admin.changePassword(businessID, info);
        return Response.ok("Password updated").build();
    }
}
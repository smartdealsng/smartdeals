package com.smartdealsng.business.partners.boundary;

import com.smartdealsng.business.authentication.boundary.InvalidDetailsException;
import com.smartdealsng.business.authentication.control.AuthenticationStore;
import com.smartdealsng.business.authentication.control.PasswordEncoder;
import com.smartdealsng.business.authentication.model.PasswordInfo;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import com.smartdealsng.business.partners.entity.BusinessInformation;
import com.smartdealsng.business.partners.entity.LiveBusiness;
import static com.smartdealsng.business.partners.entity.LiveBusiness.FIND_BY_ID;
import com.smartdealsng.business.partners.entity.Discount;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * This class acts as a boundary to the {@link LiveBusinessResource}
 *
 * @author tunji@smartdeals.com.ng
 */
@Stateless
public class BusinessAdmin {
    
    @PersistenceContext
    EntityManager manager;
    
    @Inject
    PasswordEncoder encoder;
    
    @Inject
    AuthenticationStore authStore;
    
    @Inject
    Logger logger;
    
    
   public void updateLiveBusinessDocument(String businessID, BusinessInformation information) {
       final LiveBusiness liveBusiness = findLiveBusinessByID(businessID);
       final SecurityInfo securityInfo = this.authStore.findByUsername(liveBusiness.getEmail());
       logger.info("updating business information with ID: " + businessID);
       securityInfo.setUsername(information.email);
       liveBusiness.setBusinessName(information.businessName);
       liveBusiness.setRcNumber(information.rcNumber);
       liveBusiness.setCity(information.city);
       liveBusiness.setCountry(information.country);
       liveBusiness.setBusinessDescription(information.description);
       liveBusiness.setInstagramHandle(information.instagramHandle);
       liveBusiness.setPhoneNumber(information.phoneNumber);
       liveBusiness.setEmail(information.email);
       liveBusiness.setBusinessAddress(information.businessAddress);
       liveBusiness.setBusinessCategory(information.businessCategory);
       logger.info("business information updated successfully");
    }
    
    public LiveBusiness findLiveBusinessByID(String businessID) {
       logger.info("retrieving Live Business with ID: " + businessID);
       final List<LiveBusiness> liveBusinesses = this.manager
               .createNamedQuery(FIND_BY_ID, LiveBusiness.class)
                .setParameter("businessID", businessID)
                .setMaxResults(1)
                .getResultList();
        if (liveBusinesses.isEmpty()) {
            logger.severe("unable to find a Live Business associated with ID: " + businessID);
            throw new WebApplicationException(Response.status(Status.NO_CONTENT).build());
        }
        return liveBusinesses.get(0);
    }
    
    public void uploadImageOrDefault(String businessID, InputStream in) {
        logger.info("saving new image for business with id: " + businessID);
        final LiveBusiness liveBusiness = findLiveBusinessByID(businessID);
        final String saveDir = System.getProperty("user.home") + File.separator + "partners";
        final File file = FileSystems.getDefault().getPath(saveDir).toFile();
        if (!file.isDirectory()) {
            file.mkdir();
        }
        final File targetFile = new File(saveDir + File.separator + businessID + ".png");
        if (targetFile.exists()) {
            targetFile.delete();
        }
        int bytesRead;
        try (final FileOutputStream outputStream = new FileOutputStream(targetFile)) {
            final byte[] buffer = new byte[8 * 1024];
            while ((bytesRead = in.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        } catch(IOException ex) {
            logger.severe("failed to save image: " + ex.getMessage());
        }
        liveBusiness.setImageURL(targetFile.getPath());
    }
      
    public String addDiscount(String businessID, Discount discount) {
        logger.info("adding new discount for business with id: " + businessID);
        final LiveBusiness liveBusiness = findLiveBusinessByID(businessID);
        final Discount existingDiscount = this.manager.find(Discount.class, liveBusiness.getId());
        if (existingDiscount != null) {
            logger.info("existing discount found, updating...");
            existingDiscount.setActive(discount.getStatus());
            existingDiscount.setDiscountOffered(discount.getDiscountOffered());
            existingDiscount.setMinimumSpend(discount.getMinimumSpend());
            liveBusiness.setAccountEnabled(discount.getStatus());
            liveBusiness.setPartnerStatus();
            return existingDiscount.getStatus().toString();
        }
        discount.setAssociatedLiveBusiness(liveBusiness);
        this.manager.persist(discount);
        liveBusiness.setAccountEnabled(discount.getStatus());
        liveBusiness.setPartnerStatus();
        logger.info("discount added");
        return discount.getStatus().toString();
    }
    
    public void changePassword(String businessID, PasswordInfo info) {
        final LiveBusiness business = findLiveBusinessByID(businessID);
        logger.info("received password change request for Live Business" + businessID);
        final String oldPassword = info.oldPassword;
        final String newPassword = info.newPassword;
        final String confirmPassword = info.confirmPassword;
        if (!newPassword.equals(confirmPassword)) {
            logger.severe("password mismatch");
            throw new InvalidDetailsException("Password mismatch");
        }
        final SecurityInfo securityInfo = this.authStore.getSecurityInfo(business.getEmail(), oldPassword);
        if (!encoder.digestPassword(oldPassword).equals(securityInfo.getPassword())) {
            logger.severe("invalid old password for Live Business" + businessID);
            throw new InvalidDetailsException("Invalid Old password");
        }
        if (encoder.digestPassword(oldPassword).equals(encoder.digestPassword(newPassword))) {
            logger.severe("the new password has already been used");
            throw new InvalidDetailsException("Already used pin");
        }
        securityInfo.setPassword(encoder.digestPassword(newPassword));
    }
    
}

package com.smartdealsng.business.health.boundary;

import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
@Health
@ApplicationScoped
public class SmartDealsHealthCheck implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("SMARTDEALS DISCOUNT HUB")
                .up()
                .withData("info", "ready to receive requests")
                .build();
    }
    
}

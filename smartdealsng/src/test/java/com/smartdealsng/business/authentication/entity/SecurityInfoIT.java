package com.smartdealsng.business.authentication.entity;

import com.smartdealsng.business.AbstractPersistence;
import com.smartdealsng.business.TestUtils;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class SecurityInfoIT extends AbstractPersistence {
    
    @Test
    public void validateORM() {
        final SecurityInfo securityInfo = TestUtils.newSecurityInfo();
        this.transaction.begin();
        this.manager.persist(securityInfo);
        this.transaction.commit();
        assertThat(securityInfo.getId(), is(1L));
        
    }
    
}

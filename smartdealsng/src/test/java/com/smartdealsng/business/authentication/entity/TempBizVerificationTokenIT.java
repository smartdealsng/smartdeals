package com.smartdealsng.business.authentication.entity;

import com.smartdealsng.business.AbstractPersistence;
import com.smartdealsng.business.TestUtils;
import com.smartdealsng.business.authentication.model.TempBizVerificationToken;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class TempBizVerificationTokenIT extends AbstractPersistence {

    @Test
    public void validateORM() {
        final TemporaryBusiness tempBiz = TestUtils.temporaryBusiness();
        this.transaction.begin();
        this.manager.persist(tempBiz);
        this.transaction.commit();
        assertThat(tempBiz.getId(), is(1L));

        final TempBizVerificationToken verificationToken = TestUtils.newVerificationToken();
        verificationToken.setTempBiz(tempBiz);
        this.transaction.begin();
        this.manager.persist(verificationToken);
        this.transaction.commit();
        assertThat(verificationToken.getId(), is(1L));

        this.transaction.begin();
        this.manager.remove(verificationToken);
        this.transaction.commit();

        this.transaction.begin();
        final TemporaryBusiness found = this.manager.find(TemporaryBusiness.class, 1L);
        this.transaction.commit();
        assertNotNull(found);

        this.transaction.begin();
        final TempBizVerificationToken verifFound = this.manager.find(TempBizVerificationToken.class, 1L);
        this.transaction.commit();
        assertNull(verifFound);
    }
}

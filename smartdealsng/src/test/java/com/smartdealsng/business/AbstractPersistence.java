package com.smartdealsng.business;

import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public abstract class AbstractPersistence {

    protected static EntityManagerFactory factory = Persistence.createEntityManagerFactory("integration-test");
    protected EntityManager manager;
    protected EntityTransaction transaction;
    protected Validator validator;

    @Before
    public void setup() {
        this.manager = factory.createEntityManager();
        this.transaction = manager.getTransaction();
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        this.validator = vf.getValidator();
    }

    @After
    public void tearDown() throws SQLException {
        this.transaction.begin();
        this.manager.createQuery("DELETE FROM Discount d").executeUpdate();
        this.manager.createQuery("DELETE FROM LiveBusiness l").executeUpdate();
        this.manager.createQuery("DELETE FROM TempBizVerificationToken t").executeUpdate();
        this.manager.createQuery("DELETE FROM TemporaryBusiness t").executeUpdate();
        this.manager.createQuery("DELETE FROM SecurityInfo s").executeUpdate();
        this.transaction.commit();
    }

}

package com.smartdealsng.business;

import com.smartdealsng.business.authentication.control.PasswordEncoder;
import com.smartdealsng.business.authentication.model.SecurityInfo;
import com.smartdealsng.business.authentication.model.TempBizVerificationToken;
import com.smartdealsng.business.partners.entity.LiveBusiness;
import com.smartdealsng.business.partners.entity.Discount;
import com.smartdealsng.business.partners.entity.DiscountStatus;
import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 *
 * @author user
 */
public class TestUtils {
    
    private static PasswordEncoder encoder = new PasswordEncoder();
    
    public static LiveBusiness newLiveBusiness() {
        final LiveBusiness liveBusiness = new LiveBusiness();
        liveBusiness.setEmail("olatunji4you@gmail.com");
        liveBusiness.setBusinessID("homes.ng/012019/01A");
        liveBusiness.setPhoneNumber("08025903692");
        liveBusiness.setBusinessAddress("apele inside shagamu");
        liveBusiness.setBusinessCategory("real estate");
        liveBusiness.setBusinessDescription("real estate dealer");
        liveBusiness.setBusinessName("homes.ng");
        liveBusiness.setRcNumber("ahfiihahia--fahala");
        liveBusiness.setBusinessType("buying and selling");
        liveBusiness.setCity("shagamu");
        liveBusiness.setState("ogun state");
        liveBusiness.setCrmCode("xnsDhfGHKAdjdhOOxxxxhnka");
        liveBusiness.setCountry("NIGERIA");
        liveBusiness.setInstagramHandle("@homes.ng");
        liveBusiness.setVerificationCode(UUID.randomUUID().toString());
        liveBusiness.setRegisteredAt(LocalDateTime.now());
        liveBusiness.setAccountActive();
        liveBusiness.setPartnerStatus();
        return liveBusiness;
    }
    
    public static TempBizVerificationToken newVerificationToken() {
        final TempBizVerificationToken verificationToken = new TempBizVerificationToken(UUID.randomUUID().toString(),
                LocalDateTime.now().plusMinutes(30));
        return verificationToken;
    }
    
    public static TemporaryBusiness temporaryBusiness() {
        final TemporaryBusiness temporaryBusiness = new TemporaryBusiness();
        temporaryBusiness.setBusinessName("homes.ng");
        temporaryBusiness.setEmail("olatunji4you@gmail.com");
        temporaryBusiness.setPhone("08025903692");
        temporaryBusiness.setCrmCode("87827hfakjaoijfnkala");
        temporaryBusiness.setRegistrationDate(LocalDateTime.now());
        return temporaryBusiness;
    }
    
    public static Discount newDiscount() {
        final Discount discount = new Discount();
        discount.setActive(DiscountStatus.ACTIVE);
        discount.setDiscountOffered(30);
        discount.setMinimumSpend("minimum of 10,000");
        return discount;
    }
    
    public static SecurityInfo newSecurityInfo() {
        final SecurityInfo info = new SecurityInfo("Olatunji4you@gmail.com", encoder.digestPassword("Olatunji4you@gmail.com"), 
        UUID.randomUUID().toString());
        return info;
    }
    
}

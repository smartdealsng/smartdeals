package com.smartdealsng.business.partners.entity;

import com.smartdealsng.business.partners.entity.LiveBusiness;
import com.smartdealsng.business.TestUtils;
import com.smartdealsng.business.AbstractPersistence;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class LiveBusinessIT extends AbstractPersistence {
    
    @Test
    public void validateORM() {
        this.transaction.begin();
        LiveBusiness liveBusiness = TestUtils.newLiveBusiness();
        this.manager.persist(liveBusiness);
        this.transaction.commit();
        assertThat(liveBusiness.getId(), is(1L));
    }    
}

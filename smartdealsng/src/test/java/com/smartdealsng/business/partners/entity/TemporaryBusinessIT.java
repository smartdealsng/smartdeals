package com.smartdealsng.business.partners.entity;

import com.smartdealsng.business.partners.entity.TemporaryBusiness;
import com.smartdealsng.business.AbstractPersistence;
import com.smartdealsng.business.TestUtils;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.ConstraintViolation;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class TemporaryBusinessIT extends AbstractPersistence {
   
    @Test
    public void validateORM() {
        this.transaction.begin();
        TemporaryBusiness tempBiz = TestUtils.temporaryBusiness();
        this.manager.persist(tempBiz);
        this.transaction.commit();
        assertThat(tempBiz.getId(), is(1L));
    }
    
    @Test
    public void incorrectEmailShouldThrowConstraintViolated() {
        TemporaryBusiness tempBiz = new TemporaryBusiness("homes.ng", "olatunj4youcom", "08025903692", LocalDateTime.now(), "----jabjahuwuhuahuhrbbjaaKHAhjbAJHKA--");
        Set<ConstraintViolation<TemporaryBusiness>> violations = this.validator.validate(tempBiz);
        assertFalse(violations.isEmpty());
    }
    
}

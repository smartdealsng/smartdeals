package com.smartdealsng.business.partners.entity;

import com.smartdealsng.business.partners.entity.LiveBusiness;
import com.smartdealsng.business.partners.entity.Discount;
import com.smartdealsng.business.AbstractPersistence;
import com.smartdealsng.business.TestUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class DiscountIT extends AbstractPersistence {
    
    @Test
    public void createNewDiscountWithLiveBusiness() {
        final LiveBusiness business = TestUtils.newLiveBusiness();
        final Discount discount = TestUtils.newDiscount();
        discount.setAssociatedLiveBusiness(business);
        this.transaction.begin();
        this.manager.persist(business);
        this.transaction.commit();
        assertThat(business.getId(), is(1L));
        
        this.transaction.begin();
        this.manager.persist(discount);
        this.transaction.commit();
        
        this.transaction.begin();
        final Discount associated = this.manager.find(Discount.class, 1L);
        this.transaction.commit();
        assertNotNull(associated);
        assertThat(associated.getDiscountOffered(), is(30));
        
//        this.transaction.begin();
//        this.manager.remove(discount);
//        this.manager.remove(business);
//        this.transaction.commit();
    }
    
}

package com.smartdealsng.business.authentication.boundary;

import javax.ws.rs.core.Response;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class RegistrationsResourceIT {
    
    private RegistrationsResourceSupport resourceSupport;

    @Before
    public void initClient() {
        this.resourceSupport = new RegistrationsResourceSupport();
    }

    //@Test
    public void registerTemporaryBusiness() {
        final Response response = this.resourceSupport.registerTemporaryBusiness();
        assertEquals(200, response.getStatus());
    }

    //@Test
    public void registerTemporaryBusinessInvalidEmail() {
        final Response response = this.resourceSupport.registerTemporaryBusinessInvalidEmail();
        assertNotNull(response.getHeaderString("x-cause"));
    }
    
    @Test
    public void verifyRegistrationEmail() {
        final Response response = this.resourceSupport.verifyEmail();
        final String businessID = response.readEntity(String.class);
        assertNotNull(businessID);
        System.out.println(businessID);
        
    }
    
    //@Test
    public void login() {
        final Response response = this.resourceSupport.login();
        final String authorizationHeader = response.getHeaderString("Authorization");
        assertNotNull(authorizationHeader);
    }
}

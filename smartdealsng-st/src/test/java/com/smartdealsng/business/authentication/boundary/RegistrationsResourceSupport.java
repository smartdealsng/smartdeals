package com.smartdealsng.business.authentication.boundary;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.logging.LoggingFeature;

/**
 *
 * @author user
 */
public class RegistrationsResourceSupport {
    
    private final static String RUT_URI = "http://localhost:8080/smartdealsng/resources/auth";
    private Client client;
    private WebTarget registrationsTarget;

    public RegistrationsResourceSupport() {
        this.client = ClientBuilder.newBuilder().
                connectTimeout(100, TimeUnit.SECONDS).
                readTimeout(100, TimeUnit.SECONDS).register(loggin()).
                build();
        this.registrationsTarget = client.target(RUT_URI);
    }
    
    LoggingFeature loggin() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        return new LoggingFeature(logger, Level.INFO, null, null);
    }

    public Response registerTemporaryBusiness() {
        final Response response = this.registrationsTarget.path("register").
                request(APPLICATION_JSON).
                post(Entity.json(registrationDetails()));
        return response;
    }

    public Response registerTemporaryBusinessInvalidEmail() {
        final Response response = this.registrationsTarget.path("register").
                request(APPLICATION_JSON).
                post(Entity.json(registrationDetailsWithInvalidEmail()));
        return response;

    }
    
    public Response verifyEmail() {
        final Response response = this.registrationsTarget.path("/verify/{token}")
                .resolveTemplate("token", "87940384-6d1f-4876-9383-48be1fcc439f")
                .request()
                .get();
        return response;
    }
    
    public Response login() {
        final Response response = this.registrationsTarget.path("/login")
                .request()
                .post(Entity.json(loginDetails()));
        return response;
    }

   JsonObject registrationDetailsWithInvalidEmail() {
        return Json.createObjectBuilder()
                .add("businessName", "homesng")
                .add("email", "Olatunji4you")
                .add("phone", "08025903692")
                .add("crm_code", "---jahaii2hanhahJHFlaha--")
                .build();
    }

   JsonObject registrationDetails() {
        return Json.createObjectBuilder()
                .add("businessName", "homesng")
                .add("email", "olatunji4you@gmail.com")
                .add("phone", "08025903692")
                .add("crm_code", "---jahaii2hanhahJHFlaha--")
                .build();
    }
    
   JsonObject loginDetails() {
        return Json.createObjectBuilder()
                .add("username", "olatunji4you@gmail.com")
                .add("password", "olatunji4you@gmail.com")
                .build();
    }
}

package com.smartdealsng.business.partners.boundary;

import java.io.FileNotFoundException;
import javax.ws.rs.core.Response;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class LiveBusinessResourceIT {
    
    private LiveBusinessResourceSupport liveBusinessResourceSupport;
    
    @Before
    public void initClient() {
        this.liveBusinessResourceSupport = new LiveBusinessResourceSupport();
    }
    
    @Test
    public void updateBusinessInformation() {
        Response response = liveBusinessResourceSupport.uploadBusinessInformation();
        assertNotNull(response);
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void updateDiscount() {
        Response response = liveBusinessResourceSupport.updateDiscount();
        String status = response.readEntity(String.class);
        assertEquals(status, "ACTIVE");
        assertNotNull(response);
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void updateBusinessDocument() throws FileNotFoundException  {
        Response response = liveBusinessResourceSupport.uploadBusinessDocument();
        final String message = response.readEntity(String.class);
        System.out.println(message);
        assertNotNull(response);
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void changePassword() {
        Response response = liveBusinessResourceSupport.changePassword();
        assertEquals(response.getStatus(), 200);
    }
    
    @Test
    public void accessUnauthorized() {
        Response response = liveBusinessResourceSupport.accessResourceNotAuthorized();
        assertEquals(response.getStatus(), 401);
        
    }
}

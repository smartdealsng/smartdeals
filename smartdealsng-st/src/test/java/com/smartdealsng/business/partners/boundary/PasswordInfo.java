package com.smartdealsng.business.partners.boundary;

/**
 *This class acts as a DTO for transporting credentials involved when making
 * a request for a password change.
 * 
 * @author tunji@smartdeals.com.ng
 */
public class PasswordInfo {
    
    public String oldPassword;
    public String newPassword;
    public String confirmPassword;
    
    public PasswordInfo() {
        
    }
    
    public PasswordInfo(String oldPassword, String newPassword, String confirmPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }
    
}

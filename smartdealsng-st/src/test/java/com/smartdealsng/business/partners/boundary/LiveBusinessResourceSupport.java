package com.smartdealsng.business.partners.boundary;

import com.smartdealsng.business.authentication.boundary.RegistrationsResourceSupport;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.logging.LoggingFeature;

/**
 *
 * @author tunji@smartdeals.com.ng
 */
public class LiveBusinessResourceSupport {

    private static final String RUT_URI = "http://localhost:8080/smartdealsng/resources/business";
    private final Client client;
    private final WebTarget liveBusinessResourceTarget;
    private final RegistrationsResourceSupport registrationsresourceSupport;
    private final String authorizationHeader;
    private final String businessID = "homesng20191QM";

    public LiveBusinessResourceSupport() {
        this.registrationsresourceSupport = new RegistrationsResourceSupport();
        this.client = ClientBuilder.newBuilder().
                connectTimeout(100, TimeUnit.SECONDS).
                readTimeout(100, TimeUnit.SECONDS).register(loggin()).
                build();
        this.liveBusinessResourceTarget = client.target(RUT_URI);
        Response loginResponse = this.registrationsresourceSupport.login();
        this.authorizationHeader = loginResponse.getHeaderString("Authorization");
    }

    LoggingFeature loggin() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        return new LoggingFeature(logger, Level.INFO, null, null);
    }

    public Response uploadBusinessInformation() {
        Response response = this.liveBusinessResourceTarget.path("/{businessID}")
                .resolveTemplate("businessID", businessID)
                .request(APPLICATION_JSON)
                .header("Authorization", authorizationHeader)
                .put(Entity.json(businessInformation()));
        return response;
    }
    
    public Response uploadBusinessDocument() throws FileNotFoundException {
        final InputStream stream = this.getClass().getResourceAsStream("/logo.png");
        Response response = this.liveBusinessResourceTarget.path("/{businessID}/upload")
                .resolveTemplate("businessID", businessID)
                .request()
                .header("Authorization", authorizationHeader)
                .post(Entity.entity(stream, APPLICATION_OCTET_STREAM));
        return response;
    }
    
    public Response updateDiscount() {
        Response response = this.liveBusinessResourceTarget.path("/{businessID}/discount")
                .resolveTemplate("businessID", businessID)
                .request(APPLICATION_JSON)
                .header("Authorization", authorizationHeader)
                .post(Entity.json(discount()));
        return response;
    }
    
    public Response changePassword() {
        Response response = this.liveBusinessResourceTarget.path("/{businessID}/changepassword")
                .resolveTemplate("businessID", businessID)
                .request(APPLICATION_JSON)
                .header("Authorization", authorizationHeader)
                .put(Entity.json(passwordInformation()));
        return response;
    }
    
    String passwordInformation() {
        final PasswordInfo info = new PasswordInfo("olatunji4you@gmail.com", "ciaoforever", "ciaoforever");
        final Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        return jsonb.toJson(info);
    }
    
    public Response accessResourceNotAuthorized() {
        Response response = this.liveBusinessResourceTarget.path("/{businessID}")
                .resolveTemplate("businessID", businessID)
                .request(APPLICATION_JSON)
                .put(Entity.json(businessInformation()));
        return response;
    }
    
    String businessInformation() {
        final BusinessInformation info = new BusinessInformation("homesng", "23789728829", "olatunji4you@gmail.com", "08025903692", "Apele, shagamu", 
        "@homes.ng", "shagamu", "ogun state", "nigeria", "real estate", "housing and furnishing", "we sell and buy houses");
        final Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withFormatting(true));
        return jsonb.toJson(info);
    }
    
    JsonObject discount() {
        return Json.createObjectBuilder()
                .add("status", "ACTIVE")
                .add("discountOffered", 30)
                .add("minimum_spend", "minimum spend of 5000")
                .build();
    }
}
